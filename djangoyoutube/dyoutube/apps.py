import urllib.request
from django.apps import AppConfig
from .ytparser import YTChannel


class DyoutubeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dyoutube'

    def ready(self):
        from .models import ContenidoVid
        # Aquí pondríamos el canal que quisieramos:
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        canal = YTChannel(xmlStream)
        videos = canal.videos()
        obj = ContenidoVid.objects.all()
        obj.delete()
        # introducimos en el model ContenidoVid el contenido de los videos:
        for vid in videos:
            v = ContenidoVid(Titulo=vid['title'],
                       ID_vid=vid['id_video'],
                       Seleccionado=False,
                       url=vid['url'])

            v.save()
