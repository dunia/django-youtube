from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.http import Http404
from .models import ContenidoVid
from django.views.decorators.csrf import csrf_exempt


# Create your views here.

@csrf_exempt
def index(request):
    if request.method == "POST":
        boton = request.POST['boton']
        id = request.POST['id']
        video = ContenidoVid.objects.get(ID_vid=id)

        if boton == "Eliminar":
            video.Seleccionado = False
        elif boton == "Seleccionar":
            video.Seleccionado = True

        video.save()


    lista = ContenidoVid.objects.all()
    template = loader.get_template('dyoutube/index.html')
    context = {
        'content_list': lista
    }
    return HttpResponse(template.render(context, request))


@csrf_exempt
def error():
    template = loader.get_template('dyoutube/error.html')
    context = {}
    return Http404(template.render(context))